$(warning "Setting TwoWheeler Build Params")
TARGET_BUILD_2W := true
TARGET_BOARD_TYPE := 2w

# Disable unsupported feature
PRODUCT_COPY_FILES += \
    device/qcom/bengal_2w/permissions/bike_excluded_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/bike_excluded_hardware.xml \
    device/qcom/bengal_2w/displayconfig/display_id_4630946722588367745.xml:$(TARGET_COPY_OUT_VENDOR)/etc/displayconfig/display_id_4630946722588367745.xml \
    device/qcom/bengal_2w/powerprofile/system_power_policy_battery_profile_critical_on.xml:$(TARGET_COPY_OUT_VENDOR)/etc/automotive/system_power_policy_battery_profile_critical_on.xml \
    device/qcom/bengal_2w/powerprofile/system_power_policy_battery_profile_low_on.xml:$(TARGET_COPY_OUT_VENDOR)/etc/automotive/system_power_policy_battery_profile_low_on.xml \
    device/qcom/bengal_2w/ecallconfig/msdsettings.txt:$(TARGET_COPY_OUT_VENDOR)/etc/msdsettings.txt \
    device/qcom/bengal_2w/ecallconfig/updated_msdsettings.txt:$(TARGET_COPY_OUT_VENDOR)/etc/updated_msdsettings.txt

