#Call Twowheeler mk to set flags & copy etc files needed
#$(call inherit-product, device/qcom/bengal/twowheeler.mk)
-include $(TOPDIR)device/qcom/bengal_2w/twowheeler.mk

# Enable AVB 2.0
BOARD_AVB_ENABLE := true

# Enable Virtual A/B
ENABLE_VIRTUAL_AB := true

ifeq ($(ENABLE_VIRTUAL_AB), true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif

# Default A/B configuration
ENABLE_AB ?= true

SYSTEMEXT_SEPARATE_PARTITION_ENABLE = true

# Enable Dynamic partition
BOARD_DYNAMIC_PARTITION_ENABLE ?= true

SHIPPING_API_LEVEL := 30
PRODUCT_SHIPPING_API_LEVEL := $(SHIPPING_API_LEVEL)

BOARD_SHIPPING_API_LEVEL := 30
BOARD_API_LEVEL := 30

# For QSSI builds, we should skip building the system image. Instead we build the
# "non-system" images (that we support).

PRODUCT_BUILD_SYSTEM_IMAGE := false
PRODUCT_BUILD_SYSTEM_OTHER_IMAGE := false
PRODUCT_BUILD_VENDOR_IMAGE := true
PRODUCT_BUILD_PRODUCT_IMAGE := false
PRODUCT_BUILD_PRODUCT_SERVICES_IMAGE := false
PRODUCT_BUILD_ODM_IMAGE := false
ifeq ($(ENABLE_AB), true)
PRODUCT_BUILD_CACHE_IMAGE := false
else
PRODUCT_BUILD_CACHE_IMAGE := true
endif
PRODUCT_BUILD_RAMDISK_IMAGE := true
PRODUCT_BUILD_USERDATA_IMAGE := true

TARGET_SKIP_OTA_PACKAGE := true

#BUILD_BROKEN_PHONY_TARGETS := true
BUILD_BROKEN_DUP_RULES := true
TEMPORARY_DISABLE_PATH_RESTRICTIONS := true
#export TEMPORARY_DISABLE_PATH_RESTRICTIONS

ifneq ($(strip $(BOARD_DYNAMIC_PARTITION_ENABLE)),true)
# Enable chain partition for system, to facilitate system-only OTA in Treble.
BOARD_AVB_SYSTEM_KEY_PATH := external/avb/test/data/testkey_rsa2048.pem
BOARD_AVB_SYSTEM_ALGORITHM := SHA256_RSA2048
BOARD_AVB_SYSTEM_ROLLBACK_INDEX := 0
BOARD_AVB_SYSTEM_ROLLBACK_INDEX_LOCATION := 2
else
PRODUCT_USE_DYNAMIC_PARTITIONS := true
PRODUCT_PACKAGES += fastbootd
# Add default implementation of fastboot HAL.
PRODUCT_PACKAGES += android.hardware.fastboot@1.0-impl-mock
# f2fs utilities
PRODUCT_PACKAGES += \
 sg_write_buffer \
 f2fs_io \
 check_f2fs

# Userdata checkpoint
PRODUCT_PACKAGES += \
 checkpoint_gc

ifeq ($(ENABLE_AB), true)
# Userdata checkpoint start
AB_OTA_POSTINSTALL_CONFIG += \
RUN_POSTINSTALL_vendor=true \
POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
FILESYSTEM_TYPE_vendor=ext4 \
POSTINSTALL_OPTIONAL_vendor=true
# Userdata checkpoint end
PRODUCT_COPY_FILES += $(LOCAL_PATH)/default/fstab_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.default
PRODUCT_COPY_FILES += $(LOCAL_PATH)/emmc/fstab_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc
else
PRODUCT_COPY_FILES += $(LOCAL_PATH)/default/fstab_non_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.default
PRODUCT_COPY_FILES += $(LOCAL_PATH)/emmc/fstab_non_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc
endif
BOARD_AVB_VBMETA_SYSTEM := system
BOARD_AVB_VBMETA_SYSTEM_KEY_PATH := external/avb/test/data/testkey_rsa2048.pem
BOARD_AVB_VBMETA_SYSTEM_ALGORITHM := SHA256_RSA2048
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX := $(PLATFORM_SECURITY_PATCH_TIMESTAMP)
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX_LOCATION := 2
$(call inherit-product, build/make/target/product/gsi_keys.mk)
endif

BOARD_HAVE_BLUETOOTH := false
BOARD_HAVE_QCOM_FM := false
TARGET_DISABLE_PERF_OPTIMIATIONS := false

TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

# Enable incremental FS feature
PRODUCT_PROPERTY_OVERRIDES += ro.incremental.enable=1

# set socid related props directly instead of calling
# init.qti.early_init.sh from on early-init
# These will need to be set at early_init by
# init.qti.early_init.sh only to support multiple SOCs
#PRODUCT_PROPERTY_OVERRIDES += \
#   ro.vendor.qti.soc_id=473 \
#   ro.vendor.qti.soc_model=QCM2290


PRODUCT_PACKAGES += init.qti.early_init.sh
PRODUCT_PROPERTY_OVERRIDES += \
    ro.soc.manufacturer=QTI

# privapp-permissions whitelisting (To Fix CTS :privappPermissionsMustBeEnforced)
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

TARGET_DEFINES_DALVIK_HEAP := true

$(call inherit-product, device/qcom/vendor-common/common64.mk)
# Temporary bring-up config <--

# Temporary bring-up config -->
PRODUCT_SUPPORTS_VERITY := false
# Temporary bring-up config <--
###########
PRODUCT_PROPERTY_OVERRIDES  += \
     dalvik.vm.heapstartsize=8m \
     dalvik.vm.heapsize=256m \
     dalvik.vm.heapgrowthlimit=128m \
     dalvik.vm.heaptargetutilization=0.75 \
     dalvik.vm.heapminfree=512k \
     dalvik.vm.heapmaxfree=8m

PRODUCT_PROPERTY_OVERRIDES += persist.vendor.rcs.singlereg.feature = 1

# Target naming
PRODUCT_NAME := bengal_2w
PRODUCT_DEVICE := bengal_2w
PRODUCT_BRAND := qti
PRODUCT_MODEL := Bengal 2W for arm64


TARGET_USES_AOSP := false
TARGET_USES_AOSP_FOR_AUDIO := false
TARGET_USES_QCOM_BSP := false

# RRO configuration
TARGET_USES_RRO := true

TARGET_DISABLE_DISPLAY := false

# Kernel configurations
TARGET_KERNEL_VERSION := 4.19
#Enable llvm support for kernel
KERNEL_LLVM_SUPPORT := true
#Enable sd-llvm support for kernel
KERNEL_SD_LLVM_SUPPORT := true

###########
# Target configurations

QCOM_BOARD_PLATFORMS += bengal

TARGET_USES_QSSI := true

#Default vendor image configuration
ENABLE_VENDOR_IMAGE := true

# default is nosdcard, S/W button enabled in resource
PRODUCT_CHARACTERISTICS := nosdcard

BOARD_FRP_PARTITION_NAME := frp

# Android EGL implementation
PRODUCT_PACKAGES += libGLES_android

PRODUCT_PACKAGES += fs_config_files
PRODUCT_PACKAGES += gpio-keys.kl
PRODUCT_PACKAGES += libvolumelistener

ifeq ($(ENABLE_AB), true)
# A/B related packages
PRODUCT_PACKAGES += update_engine \
    update_engine_client \
    update_verifier \
    android.hardware.boot@1.1-impl-qti \
    android.hardware.boot@1.1-impl-qti.recovery \
    android.hardware.boot@1.1-service

PRODUCT_HOST_PACKAGES += \
    brillo_update_payload
# Boot control HAL test app
PRODUCT_PACKAGES_DEBUG += bootctl

PRODUCT_PACKAGES += \
  update_engine_sideload

endif
DEVICE_FRAMEWORK_MANIFEST_FILE := device/qcom/bengal_2w/framework_manifest.xml

DEVICE_MANIFEST_FILE := device/qcom/bengal_2w/manifest.xml
DEVICE_MATRIX_FILE   := device/qcom/common/compatibility_matrix.xml

# Default tone for sound settings
PRODUCT_PROPERTY_OVERRIDES := \
    ro.config.ringtone=Eastern_Sky.ogg \
    ro.config.notification_sound=Polaris.ogg \
    ro.config.alarm_alert=Alarm_Beep_02.ogg \
    $(PRODUCT_PROPERTY_OVERRIDES) \

# Kernel modules install path
KERNEL_MODULES_INSTALL := dlkm
KERNEL_MODULES_OUT := out/target/product/$(PRODUCT_NAME)/$(KERNEL_MODULES_INSTALL)/lib/modules

# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

#target specific runtime prop for qspm
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.qspm.enable=true

#FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# Audio configuration file
-include $(TOPDIR)vendor/qcom/opensource/audio-hal/primary-hal/configs/bengal/bengal.mk

# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

#Enable full treble flag
PRODUCT_FULL_TREBLE_OVERRIDE := true
PRODUCT_VENDOR_MOVE_ENABLED := true
PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true
BOARD_VNDK_VERSION := current
TARGET_MOUNT_POINTS_SYMLINKS := false

PRODUCT_BOOT_JARS += telephony-ext
PRODUCT_PACKAGES += telephony-ext

PRODUCT_BOOT_JARS += tcmiface

# Vendor property to enable advanced network scanning
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.radio.enableadvancedscan=true

# Property to disable ZSL mode
PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=1

# set display density to 160 for 1024x600 7inch display
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=160 \
    debug.sf.nobootanimation=1

PRODUCT_PROPERTY_OVERRIDES += \
ro.crypto.volume.filenames_mode = "aes-256-cts" \
ro.crypto.allow_encrypt_override = true

PRODUCT_PACKAGES += init.qti.dcvs.sh
PRODUCT_PACKAGES += android.hardware.lights-service.qti

#VEHICLE_NETWORKS
VEHICLE_NETWORKS += vendor.qti.hardware.automotive.vehicle@1.0-service
VEHICLE_NETWORKS += vendor.qti.hardware.automotive.vehicle@1.0-service.rc
VEHICLE_NETWORKS += android.hardware.automotive.vehicle@2.0-manager-lib
PRODUCT_PACKAGES += $(VEHICLE_NETWORKS)

# Disable NFC for Auto
TARGET_USES_NQ_NFC := false

# CAN test tools
PRODUCT_PACKAGES += libcwtest

#VEHICLE_NETWORKS
VEHICLE_NETWORKS += libcanwrapper
VEHICLE_NETWORKS += canflasher
VEHICLE_NETWORKS += libcantranslator
VEHICLE_NETWORKS += earlycan
VEHICLE_NETWORKS += esplash
PRODUCT_PACKAGES += $(VEHICLE_NETWORKS)

#----------------------------------------------------------------------
# wlan specific
#----------------------------------------------------------------------
include device/qcom/wlan/bengal/wlan.mk

#SOFTSKU
#SOFTSKU += softsku
#SOFTSKU += car2cloud.conf

#MOSQUITTO
MOSQUITTO += mosquitto
MOSQUITTO += mosquitto.conf
MOSQUITTO += libmosquitto.so

#EDGEHUB
EDGEHUB += c2c_hub
EDGEHUB += dm
EDGEHUB += cm_manager
EDGEHUB += softsku_manager
EDGEHUB += libc2cutils
EDGEHUB += libsoa
EDGEHUB += libssil
EDGEHUB += c2c_default.conf
EDGEHUB += c2c_vehicle.json
EDGEHUB += cmd_stub.conf
EDGEHUB += cmd_svc.conf
EDGEHUB += dm.conf
EDGEHUB += side_loaded.conf
EDGEHUB += sim_comm_info.json
EDGEHUB += softsku.conf
EDGEHUB += tele_service.conf
EDGEHUB += libany2any
EDGEHUB += libzstd
EDGEHUB += libedgeproc
EDGEHUB += libnotify
EDGEHUB += libvsim
EDGEHUB += libfiletx
EDGEHUB += libc2cpm
EDGEHUB += libc2c_proto
EDGEHUB += libc2c_platform
EDGEHUB += libprotobuf-cpp-full-3.9.1
EDGEHUB += libralf

EDGEHUB += libcusp

EDGEHUB += trip.conf
EDGEHUB += cacert.pem
EDGEHUB += clientcert.pem.crt
EDGEHUB += private.pem.key
EDGEHUB += normal_mode.json
EDGEHUB += vacation_mode.json
EDGEHUB += vehicle_mode.json
EDGEHUB += factory_conf.json


#PAHO
PAHO += libpaho-mqtt3c
PAHO += libpaho-mqtt3cs

#PRODUCT_PACKAGES += $(SOFTSKU)
PRODUCT_PACKAGES += $(EDGEHUB)
PRODUCT_PACKAGES += $(MOSQUITTO)
PRODUCT_PACKAGES += $(PAHO)

#SOFTSKU_DBG
#SOFTSKU_DBG += softsku
#SOFTSKU_DBG += car2cloud.conf

#EDGEHUB_DBG
EDGEHUB_DBG += c2c_hub
EDGEHUB_DBG += dm
EDGEHUB_DBG += cm_manager
EDGEHUB_DBG += softsku_manager
EDGEHUB_DBG += libc2cutils
EDGEHUB_DBG += libsoa
EDGEHUB_DBG += libssil
EDGEHUB_DBG += c2c_default.conf
EDGEHUB_DBG += c2c_vehicle.json
EDGEHUB_DBG += cmd_stub.conf
EDGEHUB_DBG += cmd_svc.conf
EDGEHUB_DBG += dm.conf
EDGEHUB_DBG += side_loaded.conf
EDGEHUB_DBG += sim_comm_info.json
EDGEHUB_DBG += softsku.conf
EDGEHUB_DBG += tele_service.conf
EDGEHUB_DBG += libany2any
EDGEHUB_DBG += libzstd
EDGEHUB_DBG += libedgeproc
EDGEHUB_DBG += libnotify
EDGEHUB_DBG += libvsim
EDGEHUB_DBG += libfiletx
EDGEHUB_DBG += libc2cpm
EDGEHUB_DBG += libc2c_proto
EDGEHUB_DBG += libc2c_platform
EDGEHUB_DBG += libprotobuf-cpp-full-3.9.1
EDGEHUB_DBG += libralf

EDGEHUB_DBG += libcusp
EDGEHUB_DBG += trip.conf
EDGEHUB_DBG += cacert.pem
EDGEHUB_DBG += clientcert.pem.crt
EDGEHUB_DBG += private.pem.key
EDGEHUB_DBG += normal_mode.json
EDGEHUB_DBG += vacation_mode.json
EDGEHUB_DBG += vehicle_mode.json
EDGEHUB_DBG += factory_conf.json

#PRODUCT_PACKAGES_DEBUG += $(SOFTSKU_DBG)
PRODUCT_PACKAGES_DEBUG += $(EDGEHUB_DBG)
PRODUCT_PACKAGES_DEBUG += $(MOSQUITTO)
PRODUCT_PACKAGES_DEBUG += $(PAHO)

#Copy MCU firmware and version
PRODUCT_PACKAGES += s32k144.bin
PRODUCT_PACKAGES += s32k144_version.txt

# Usb disable daemon
PRODUCT_PACKAGES += car_power_daemon
# Enable car power manager for LPM(LowPowerMode)
PRODUCT_PROPERTY_OVERRIDES+= persist.vendor.car.lpm=true

# Add powerpolicyclient to product package
PRODUCT_PACKAGES += libpowerpolicyclient
PRODUCT_PACKAGES += android.frameworks.automotive.powerpolicy-ndk_platform
PRODUCT_PACKAGES += android.frameworks.automotive.powerpolicy-V1-ndk_platform

# Enable powerpolicy feature for Audio
PRODUCT_PROPERTY_OVERRIDES += vendor.audio.feature.powerpolicy.enable=true
AUDIO_FEATURE_ENABLED_POWER_POLICY := true
###################################################################################
# This is the End of target.mk file.
# Now, Pickup other split product.mk files:
###################################################################################
# TODO: Relocate the system product.mk files pickup into qssi lunch, once it is up.
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/system/*.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/*.mk)
###################################################################################
